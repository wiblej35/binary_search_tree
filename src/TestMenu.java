/*
 * Purpose: Data Structures and Algorithms Lab 11
 * Status: Complete and thoroughly tested
 * Last update: 04/22/14
 * Submitted:04/23/14
 * Comment: test suite and sample run attached
 * @author: Josh Wible
 * @version: 2014.04.22
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestMenu {
	private BinarySearchTreePlus<Person, String> tree;
	
	public TestMenu() {
		tree = new BinarySearchTreePlus<Person, String>();
	}
	
	public static BufferedReader br = new BufferedReader(new InputStreamReader(
			System.in));
	
	public static void main(String[] args) throws NumberFormatException,
			IOException {
		TestMenu tm = new TestMenu();
		tm.displayOptions();
		tm.chooseOption();
	}
	
	public void chooseOption() throws IOException {
		boolean editing = true;
		while (editing) {
			int op = validateOption(); // validates option before attempt
			
			// Initialize variables
			BinarySearchTreePlus<Person, String> copy = null;
			Person p = null;
			String id = null;
			String name = null;
			
			// Execute chosen operation
			switch (op) {
			case 1:
				// Insert key in BST
				System.out.print("ID of person: ");
				id = br.readLine();
				System.out.println(id);
				System.out.print("Name of person: ");
				name = br.readLine();
				System.out.println(name);
				tree.insert(new Person(id, name));
				System.out.println(name + " has been added to the tree.");
				break;
			case 2:
				// Delete key from BST
				System.out.print("ID of person to remove: ");
				id = br.readLine();
				System.out.println(id);
				try {
					tree.delete(id);
					System.out.println(id + " has been deleted from the tree.");
				} catch (TreeException e) {
					System.out.println("Key not found.");
				}
				break;
			case 3:
				// Search for key in BST
				System.out.print("Key of person to search for: ");
				id = br.readLine();
				System.out.println(id);
				try {
					p = tree.retrieve(id);
					System.out.println(p.toString() + " found in tree.");
				} catch (NullPointerException e) {
					System.out.println("Key not found.");
				}
				break;
			case 4:
				// Display height of BST
				System.out.println("Height: " + tree.getHeight());
				break;
			case 5:
				// Display size of BST
				System.out.println("Number of nodes: " + tree.getSize());
				break;
			case 6:
				// Display content of BST in inorder
				System.out.println("Inorder:\n" + tree.toStringInorder());
				break;
			case 7:
				// Display content of BST in preorder
				System.out.println("Preorder:\n" + tree.toStringPreorder());
				break;
			case 8:
				// Display content of BST in postorder
				System.out.println("Postorder:\n" + tree.toStringPostorder());
				break;
			case 9:
				// Build copy of the tree and test it
				copy = tree.copy();
				System.out.println("Copy inorder:\n" + copy.toStringInorder());
				copy.insert(new Person("0", "0"));
				System.out.println(copy.retrieve("0") + " has been added to copy.");
				copy.insert(new Person("10", "10"));
				System.out.println(copy.retrieve("10") + " has been added to copy.");
				try {
					copy.delete("6");
					System.out.println("Item with key 6 deleted.");
				}catch(TreeException e) {
					System.out.println("Key not found.");
				}
				System.out.println("Tree inorder:\n" + tree.toStringInorder());
				System.out.println("Copy inorder:\n" + copy.toStringInorder());
				break;
			case 10:
				// Exit program
				editing = false;
				System.out.println("Goodbye.");
				break;
			}
		}
	}
	
	private int validateOption() throws IOException {
		int op = 0; // chosen option
		boolean validOp = false;
		// Check validity of selection
		while (!validOp) {
			System.out.print("\nSelect an option (1 to 10): ");
			try {
				op = Integer.parseInt(br.readLine());
				System.out.println(op);
			} catch (NumberFormatException e) {
				System.out.println("That's not a number... Try again.");
			}
			if (op >= 1 && op <= 10) {
				validOp = true;
			} else {
				System.out.println("Number out of range... Try again.");
			}
		}
		return op;
	}
	
	private void displayOptions() {
		System.out.println("\nSelect from the following menu:\n"
				+ "1. Insert key in BST.\n" + "2. Delete key from BST.\n"
				+ "3. Search fro key in BST.\n" + "4. Display height of BST.\n"
				+ "5. Display size of BST.\n"
				+ "6. Display content of BST in inorder.\n"
				+ "7. Display content of BST in preorder.\n"
				+ "8. Display content of BST in postorder.\n"
				+ "9. Build copy of the tree and test it.\n"
				+ "10. Exit Program.\n");
	}
}
