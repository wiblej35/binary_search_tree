public class Person extends KeyedItem<String> {
	private String name;
	
	public Person(String id, String name) {
		super(id);
		this.name = name;
	}
	
	public String toString() {
		return "Key: " + getKey() + ", Name: " + name;
	}
}
