/*
 * Purpose: Data Structures and Algorithms Lab 11
 * Status: Complete and thoroughly tested
 * Last update: 04/22/14
 * Submitted:04/23/14
 * Comment: test suite and sample run attached
 * @author: Josh Wible
 * @version: 2014.04.22
 */

public class BinarySearchTreePlus<T extends KeyedItem<KT>, KT extends Comparable<? super KT>>
		extends BinarySearchTree<T, KT> {
	
	protected T findLeftmost(TreeNode<T> tNode) {
		// Overrides superclass method with iterative implementation
		TreeNode<T> temp = tNode;
		while (temp.leftChild != null) {
			temp = temp.leftChild;
		}
		return temp.item;
	}
	
	protected T retrieveItem(TreeNode<T> tNode, KT searchKey) {
		// Overrides superclass method with iterative implementation
		TreeNode<T> temp = tNode;
		T treeItem = null;
		boolean done = false;
		while (temp != null && !done) {
			int result = searchKey.compareTo(temp.item.getKey());
			if (result == 0) {
				treeItem = temp.item;
				done = true;
			} else if (result < 0) {
				temp = temp.leftChild;
			} else {
				temp = temp.rightChild;
			}
		}
		return treeItem;
	}
	
	public int getHeight() {
		return getHeight(root);
	}
	
	public int getHeight(TreeNode<T> tNode) {
		int height = -1;
		if (tNode != null) {
			height = 1 + Math.max(getHeight(tNode.leftChild),
					getHeight(tNode.rightChild));
		}
		return height;
	}
	
	public int getSize() {
		return getSize(root);
	}
	
	public int getSize(TreeNode<T> tNode) {
		int size = 0;
		if (tNode != null) {
			size = 1 + getSize(tNode.leftChild) + getSize(tNode.rightChild);
		}
		return size;
	}
	
	public String toStringPreorder() {
		return preorder(root);
	}
	
	protected String preorder(TreeNode<T> tNode) {
		String output = tNode.item.toString() + "\n";
		if (tNode.leftChild != null) {
			output += preorder(tNode.leftChild);
		}
		if (tNode.rightChild != null) {
			output += preorder(tNode.rightChild);
		}
		return output;
	}
	
	public String toStringInorder() {
		return inorder(root);
	}
	
	protected String inorder(TreeNode<T> tNode) {
		String output = tNode.item.toString() + "\n";
		if (tNode.leftChild != null) {
			output = inorder(tNode.leftChild) + output;
		}
		if (tNode.rightChild != null) {
			output += inorder(tNode.rightChild);
		}
		return output;
	}
	
	public String toStringPostorder() {
		return postorder(root);
	}
	
	protected String postorder(TreeNode<T> tNode) {
		String output = tNode.item.toString() + "\n";
		if (tNode.rightChild != null) {
			output = postorder(tNode.rightChild) + output;
		}
		if (tNode.leftChild != null) {
			output = postorder(tNode.leftChild) + output;
		}
		return output;
	}
	
	public BinarySearchTreePlus<T, KT> copy() {
		BinarySearchTreePlus<T, KT> copy = new BinarySearchTreePlus<T, KT>();
		copy.root = copy(root);
		return copy;
	}
	
	protected TreeNode<T> copy(TreeNode<T> tNode) {
		TreeNode<T> copy = null;
		if (tNode != null) {
			copy = new TreeNode<T>(tNode.item, copy(tNode.leftChild),
					copy(tNode.rightChild));
		}
		return copy;
	}
}
